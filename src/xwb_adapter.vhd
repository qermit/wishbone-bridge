-------------------------------------------------------------------------------
-- Title      :
-- Project    : Wishbone Wishbone
-------------------------------------------------------------------------------
-- File       : pcie3_tlp_interconnect.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-10-14
-- Last update: 2020-10-14
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2020 Piotr Miedzik
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

use work.wishbone_bridge_pkg.all;

entity xwb_adapter is

  generic (
    g_master_mode        : t_wishbone_interface_mode;
    g_master_granularity : t_wishbone_address_granularity;
    g_slave_mode         : t_wishbone_interface_mode;
    g_slave_granularity  : t_wishbone_address_granularity
    );
  port (
    clk_sys_i : in std_logic;
    rst_n_i   : in std_logic;

    slave_i    : in  t_wishbone_slave_in                                   := cc_dummy_slave_in;
    slave_o : out t_wishbone_slave_out;

    master_i   : in  t_wishbone_master_in                                  := cc_dummy_slave_out;
    master_o : out t_wishbone_master_out
    );
end xwb_adapter;

architecture rtl of xwb_adapter is

  function f_num_byte_address_bits
    return integer is
  begin
    case c_wishbone_data_width is
      when 8      => return 0;
      when 16     => return 1;
      when 32     => return 2;
      when 64     => return 3;
      when others =>
        report "wb_slave_adapter: invalid c_wishbone_data_width (we support 8, 16, 32 and 64)" severity failure;
    end case;
    return 0;
  end f_num_byte_address_bits;

  function f_zeros(size : integer)
    return std_logic_vector is
  begin
    return std_logic_vector(to_unsigned(0, size));
  end f_zeros;

  signal master_in  : t_wishbone_master_in;
  signal master_out : t_wishbone_master_out;
  signal slave_in   : t_wishbone_slave_in;
  signal slave_out  : t_wishbone_slave_out;
  signal master_out_adr: std_logic_vector(t_wishbone_master_out.adr'range);
  
  
begin  -- rtl
  slave_in <= slave_i;
  slave_o    <= slave_out;

  master_in <= master_i;
  master_o <= ( adr => master_out_adr,
                dat => master_out.dat,
                sel => master_out.sel,
                cyc => master_out.cyc,
                stb => master_out.stb,
                we => master_out.we) ;

  p_gen_address : process(slave_in, master_out)
  begin
    if(g_master_granularity = g_slave_granularity) then
      master_out_adr <= master_out.adr;
    elsif(g_master_granularity = BYTE) then  -- byte->word
      master_out_adr <= master_out.adr(c_wishbone_address_width-f_num_byte_address_bits-1 downto 0)
                        & f_zeros(f_num_byte_address_bits);
    else
      master_out_adr <= f_zeros(f_num_byte_address_bits)
                        & master_out.adr(c_wishbone_address_width-1 downto f_num_byte_address_bits);
    end if;
  end process p_gen_address;
  
  P2C : if (g_slave_mode = PIPELINED and g_master_mode = CLASSIC)   generate    
  begin
  
 u_p2c_adapter : entity work.xwb_adapter_p2c
  port map (
    clk_sys_i => clk_sys_i,
    rst_n_i   => rst_n_i,

    slave_i  => slave_in,
    slave_o => slave_out,

    master_i   => master_in,
    master_o   => master_out
    ); 
  end generate P2C;
  
  assert not (g_slave_mode = CLASSIC  and g_master_mode = PIPELINED) report "Unsupported mode" severity failure;  
  
  X2X : if (g_slave_mode = g_master_mode) generate
    slave_out.stall <= master_in.stall;
    slave_out.ack   <= master_in.ack;
    slave_out.err   <= master_in.err;
    slave_out.rty   <= master_in.rty;
    slave_out.dat <= master_in.dat;
      
    master_out.adr <= slave_in.adr;
    master_out.dat <= slave_in.dat;
    master_out.stb  <= slave_in.stb;

    master_out.cyc <= slave_in.cyc;
    master_out.sel <= slave_in.sel;
    master_out.we  <= slave_in.we;
  end generate X2X;
end rtl;
