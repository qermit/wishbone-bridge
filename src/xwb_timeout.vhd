-------------------------------------------------------------------------------
-- Title      :
-- Project    : Wishbone Bridge
-------------------------------------------------------------------------------
-- File       : xwb_timeout.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-30
-- Last update: 2020-09-30
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2020 Piotr Miedzik 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

entity xwb_timeout is
  generic (
    g_counter_width: natural := 8
  );
    Port ( clk_i : in STD_LOGIC;
           rst_n_i : in STD_LOGIC;

           s_input_m2s : in  t_wishbone_slave_in;
           s_input_s2m : out t_wishbone_slave_out;

           m_output_s2m : in  t_wishbone_master_in;
           m_output_m2s : out t_wishbone_master_out;
           
           timeout_counter_i: in std_logic_vector(g_counter_width-1 downto 0)
      );
end xwb_timeout;

architecture Behavioral of xwb_timeout is

  signal r_counter: unsigned(timeout_counter_i'range);
  
  type t_state is (ST_IDLE, ST_CYCLE, ST_CYCLE_STALL, ST_CYCLE_RESP, ST_DESTALL, ST_RELAX);
  signal r_state : t_state:= ST_IDLE;
begin

m_output_m2s.cyc <= s_input_m2s.cyc;
m_output_m2s.stb <= '0' when rst_n_i = '0' else 
                    '0' when r_state = ST_CYCLE_RESP else
                    '0' when r_state = ST_RELAX else    
                    '0' when r_state = ST_DESTALL else               
                    s_input_m2s.stb;
                    
m_output_m2s.adr <= s_input_m2s.adr;
m_output_m2s.sel <= s_input_m2s.sel;
m_output_m2s.we  <= s_input_m2s.we;
m_output_m2s.dat <= s_input_m2s.dat;


s_input_s2m.ack   <= '0' when r_state = ST_RELAX or r_state = ST_DESTALL else m_output_s2m.ack;
s_input_s2m.err   <= '0' when r_state = ST_RELAX or r_state = ST_DESTALL  else m_output_s2m.err;
s_input_s2m.rty   <= '1' when r_state = ST_RELAX else '0' when r_state = ST_DESTALL else m_output_s2m.rty;

s_input_s2m.stall <= '1' when rst_n_i = '0' else 
                     '1' when r_state = ST_CYCLE_RESP else
                     '1' when r_state = ST_RELAX else
                     '0' when r_state = ST_DESTALL else
                     m_output_s2m.stall;
                     
s_input_s2m.dat   <= m_output_s2m.dat;

p1: process(clk_i)
begin
  if rising_edge(clk_i) then
    case r_state is
      when ST_IDLE =>
        if s_input_m2s.cyc = '1' then
          r_state <= ST_CYCLE;
        elsif s_input_m2s.cyc = '1' and s_input_m2s.stb = '1' then
          if m_output_s2m.stall = '0' then
            r_state <= ST_CYCLE_RESP;
          else
            r_state <= ST_CYCLE_STALL;
          end if;
        end if;
      when ST_CYCLE =>
        if s_input_m2s.cyc = '0' then
          r_state <= ST_IDLE;
        elsif s_input_m2s.cyc = '1' and s_input_m2s.stb = '1' then
          if m_output_s2m.stall = '0' then
            r_state <= ST_CYCLE_RESP;
          else
            r_state <= ST_CYCLE_STALL;
          end if;
        end if;
      when ST_CYCLE_STALL =>
        if r_counter = 0 then          
           r_state <= ST_DESTALL;
        elsif s_input_m2s.cyc = '0' then 
           r_state <= ST_IDLE;
        elsif s_input_m2s.cyc = '1' and s_input_m2s.stb = '0'  then
           r_state <= ST_CYCLE;
        elsif s_input_m2s.cyc = '1' and s_input_m2s.stb = '1' and m_output_s2m.stall = '0' then
           r_state <= ST_CYCLE_RESP;
        end if;
      when ST_CYCLE_RESP =>
        if s_input_m2s.cyc = '0' then
          r_state <= ST_IDLE;
        elsif m_output_s2m.ack = '1' or m_output_s2m.rty = '1' or m_output_s2m.err = '1' then
          if s_input_m2s.stb = '1' and m_output_s2m.stall = '0'  then 
            r_state <= ST_CYCLE_RESP;
          elsif  s_input_m2s.stb = '1' and m_output_s2m.stall = '1' then
            r_state <= ST_CYCLE_STALL;
          elsif  s_input_m2s.stb = '0' then
            r_state <= ST_CYCLE;
          end if;
        elsif r_counter = 0 then
          r_state <= ST_RELAX;
        end if;
      when ST_DESTALL =>
        r_state <= ST_RELAX;
      when ST_RELAX =>
         if s_input_m2s.cyc = '0' then
          r_state <= ST_IDLE;
        elsif s_input_m2s.stb = '1' and m_output_s2m.stall = '0'  then 
                    r_state <= ST_CYCLE_RESP;
          elsif  s_input_m2s.stb = '1' and m_output_s2m.stall = '1' then
            r_state <= ST_CYCLE_STALL;
          elsif  s_input_m2s.stb = '0' then
            r_state <= ST_CYCLE;
            end if;
      when others => 
        r_state <= ST_IDLE;
        
    end case;
  
    if rst_n_i = '0' then
      r_state <= ST_IDLE;
    end if;
  end if;
  
end process;

p_counter: process(clk_i)
begin
  if rising_edge(clk_i) then
    case r_state is
      when ST_CYCLE_RESP|ST_CYCLE_STALL  =>
        if r_counter /= 0 then
          r_counter <= r_counter - 1;
        end if; 
      when others => 
        r_counter <= unsigned(timeout_counter_i);
    end case;
  
    if rst_n_i = '0' then
      r_counter <= unsigned(timeout_counter_i);
    end if;
  end if;
  
end process;


end Behavioral;
