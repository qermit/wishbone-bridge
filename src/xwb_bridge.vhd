-------------------------------------------------------------------------------
-- Title      :
-- Project    : Wishbone Bridge
-------------------------------------------------------------------------------
-- File       : xwb_bridge.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-30
-- Last update: 2020-09-30
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2020 Piotr Miedzik 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;

entity xwb_bridge is
  generic(
    g_counter_width: natural := 8;
    g_default_timeout: natural := 64
  );
  port(
    clk_i     : in  std_logic;
    rst_n_i       : in  std_logic;
    
    s_input_m2s: in  t_wishbone_slave_in;
    s_input_s2m: out t_wishbone_slave_out;
    -- Master connection to the Wishbone slaves
    m_output_m2s: out t_wishbone_master_out;
    m_output_s2m: in  t_wishbone_master_in;
    -- additional zero page 
    s_ctl_m2s: in  t_wishbone_slave_in;
    s_ctl_s2m: out t_wishbone_slave_out;
    
    counter_o: out std_logic_vector(g_counter_width-1 downto 0)
  );
end xwb_bridge;

architecture Behavioral of xwb_bridge is

signal sig_input_m2s : t_wishbone_slave_in;
signal sig_input_s2m : t_wishbone_slave_out;

signal sig_ctl_m2s : t_wishbone_slave_in;
signal sig_ctl_s2m : t_wishbone_slave_out;

signal sig_output_s2m : t_wishbone_slave_out;
signal sig_output_m2s : t_wishbone_slave_in;

signal s_ctl_addr : std_logic_vector(1 downto 0);
constant C_ADDR_STATUS: std_logic_vector(1 downto 0) := "01";
constant C_ADDR_ADDR: std_logic_vector(1 downto 0) := "00";
constant C_ADDR_COUNTER: std_logic_vector(1 downto 0) := "10";

signal r_last_op_we: std_logic;

signal r_status_wr_timeout: std_logic;
signal r_status_wr_err: std_logic;
signal r_status_rd_timeout: std_logic;
signal r_status_rd_err: std_logic;

signal r_status_addr: std_logic_vector(31 downto 0);
signal r_last_addr: std_logic_vector(31 downto 0);
signal r_last_dat: std_logic_vector(31 downto 0);
signal r_last_sel: std_logic_vector(3 downto 0);

  
  type t_state is (ST_IDLE, ST_LATCHED_OP, ST_CYCLE_IDLE, ST_CYCLE_WAIT, ST_CYCLE_STATUS, ST_CYCLE_ERR);
  signal r_state : t_state:= ST_IDLE;
  
  signal r_timout_counter: std_logic_vector(g_counter_width-1 downto 0);
begin

  counter_o <= r_timout_counter;

  s_input_s2m <= sig_input_s2m;
  sig_input_m2s <= s_input_m2s;

  m_output_m2s <= sig_output_m2s;
  sig_output_s2m <= m_output_s2m;

  sig_ctl_m2s <= s_ctl_m2s;
  s_ctl_s2m <= sig_ctl_s2m;

sig_output_m2s.cyc <= sig_input_m2s.cyc when   r_state = ST_CYCLE_STATUS or r_state = ST_CYCLE_WAIT or r_state = ST_LATCHED_OP else
--                    '0' when r_state = ST_CYCLE_ERR or  r_state = ST_CYCLE_IDLE else
                    '0';

sig_output_m2s.stb <= sig_input_m2s.stb when r_state = ST_LATCHED_OP and sig_input_m2s.adr(2) = '1' else
--                    '1' when r_state = ST_CYCLE_WAIT and sig_output_s2m.stall = '1' else 
                    '0';
                    
sig_output_m2s.adr <= r_last_addr;
sig_output_m2s.dat <= r_last_dat;
sig_output_m2s.we <= r_last_op_we;
sig_output_m2s.sel <= r_last_sel;

sig_input_s2m.dat <= sig_output_s2m.dat when r_state = ST_CYCLE_WAIT else
                   (0=>r_status_rd_err, 1=> r_status_rd_timeout, 2=>r_status_wr_err, 3=>r_status_wr_timeout, others => '0') when r_state = ST_CYCLE_STATUS else 
                   (others => '0');
                   
sig_input_s2m.ack <= '1' when r_state = ST_CYCLE_WAIT and ( sig_output_s2m.ack = '1' or sig_output_s2m.err = '1' or sig_output_s2m.rty = '1') else
                     '1' when r_state = ST_CYCLE_STATUS else
                     '0';

sig_input_s2m.rty <= '0';

sig_input_s2m.stall <= '1' when r_state = ST_CYCLE_WAIT else
                       sig_output_s2m.stall when r_state = ST_LATCHED_OP else
                       '0';
                    


p_wb_op: process(clk_i)
begin
  if rising_edge(clk_i) then
    sig_input_s2m.err <= '0';
    
    case r_state is
      when st_IDLE | ST_CYCLE_STATUS | ST_CYCLE_IDLE => 
        if sig_input_m2s.cyc = '0' then
          r_state <= st_IDLE;
        elsif sig_input_m2s.cyc = '1' and sig_input_m2s.stb = '1' and sig_input_s2m.stall = '0' then
          if sig_input_m2s.adr(2) = '0' then
            r_state <= ST_LATCHED_OP;
            r_last_addr <= "0" &  sig_input_m2s.adr(31 downto 3) & sig_input_m2s.adr(1 downto 0);
            r_last_op_we <= sig_input_m2s.we;
            r_last_sel <= sig_input_m2s.sel;
            r_last_dat <= sig_input_m2s.dat;
          elsif sig_input_m2s.adr(2) = '1' then
            r_state <= ST_CYCLE_IDLE;
            sig_input_s2m.err <= '1';                      
          end if;
        elsif sig_input_m2s.cyc = '1' then
          r_state <= ST_CYCLE_IDLE;          
        end if;
      when ST_LATCHED_OP =>
        if sig_input_m2s.cyc = '0' then
          r_state <= st_IDLE;
        elsif sig_input_m2s.cyc = '1' and sig_input_m2s.stb = '1' and sig_output_s2m.stall = '0' then
          if sig_input_m2s.adr(2) = '0' then
            r_state <= ST_CYCLE_ERR;
            r_last_addr <= "0" &  sig_input_m2s.adr(31 downto 3) & sig_input_m2s.adr(1 downto 0);
            r_last_op_we <= sig_input_m2s.we;
            sig_input_s2m.err <= '1'; -- nacknowlege to previous op
            r_last_sel <= sig_input_m2s.sel;
            r_last_dat <= sig_input_m2s.dat;
          elsif sig_input_m2s.adr(2) = '1' then
            r_state <= ST_CYCLE_WAIT;
          end if;
        end if;
      when ST_CYCLE_ERR =>
        sig_input_s2m.err <= '1';        
        if sig_input_m2s.cyc = '1' then
          r_state <= ST_CYCLE_IDLE;
        else
          r_state <= ST_IDLE;
        end if;
      when ST_CYCLE_WAIT =>
        if sig_input_m2s.cyc = '0' then
          r_state <= st_IDLE;
        elsif sig_input_m2s.cyc = '1' and ( sig_output_s2m.ack = '1' or sig_output_s2m.err = '1' or sig_output_s2m.rty = '1') then
          r_state <= ST_CYCLE_STATUS;
        end if;
    end case;

  
    if rst_n_i = '0' then
      r_last_op_we <= '0';
      r_last_addr  <= (others => '0');
      r_last_dat   <= (others => '0');
      r_last_sel   <= (others => '0');
      
      r_state      <= ST_IDLE;
    end if;
  end if;
end process;

s_ctl_addr <= sig_ctl_m2s.adr(3 downto 2);
 
p_wb_err: process(clk_i)
begin
  if rising_edge(clk_i) then
    if sig_output_m2s.cyc = '1' and sig_output_s2m.err = '1' and r_last_op_we = '1' then
      r_status_wr_err <= '1';
    end if;

    if sig_output_m2s.cyc = '1' and sig_output_s2m.err = '1' and r_last_op_we = '0' then
      r_status_rd_err <= '1';
    end if;

    if sig_output_m2s.cyc = '1' and sig_output_s2m.rty = '1' and r_last_op_we = '1' then
      r_status_wr_timeout <= '1';
    end if;

    if sig_output_m2s.cyc = '1' and sig_output_s2m.rty = '1' and r_last_op_we = '0' then
      r_status_rd_timeout <= '1';
    end if;

    if sig_output_m2s.cyc = '1' and (sig_output_s2m.rty = '1' or sig_output_s2m.err = '1') and 
       r_status_rd_timeout = '0' and r_status_wr_timeout = '0' and
       r_status_rd_err = '0' and r_status_wr_err = '0' then
       
       r_status_addr <= r_last_addr;
    end if;
    
    if sig_ctl_m2s.cyc = '1' and sig_ctl_m2s.stb = '1' and sig_ctl_m2s.we = '1' and sig_ctl_s2m.stall = '0' then
      if s_ctl_addr = C_ADDR_STATUS and sig_ctl_m2s.dat(0) = '1' then
        r_status_wr_err <= '0';
        r_status_rd_err <= '0';
        r_status_wr_timeout <= '0';
        r_status_rd_timeout <= '0';
        r_status_addr <= (others =>'0');
      end if;    
    end if;

    if sig_input_m2s.cyc = '1' and sig_input_m2s.stb = '1' and sig_input_m2s.we = '1' and sig_input_s2m.stall = '0' then
      if sig_input_m2s.adr(2) = '1' and  sig_input_m2s.dat(0) = '1' then
        r_status_wr_err <= '0';
        r_status_rd_err <= '0';
        r_status_wr_timeout <= '0';
        r_status_rd_timeout <= '0';
        r_status_addr <= (others =>'0');
      end if;    
    end if;
    
  
    if rst_n_i = '0' then
      r_status_wr_err <= '0';
      r_status_rd_err <= '0';
      r_status_wr_timeout <= '0';
      r_status_rd_timeout <= '0';
      r_status_addr <= (others =>'0');
    end if;
  end if;
end process;

p_ctl_write: process(clk_i)
begin
  if rising_edge(clk_i) then
    if sig_ctl_m2s.cyc = '1' and sig_ctl_m2s.stb = '1' and sig_ctl_s2m.stall = '0' and sig_ctl_m2s.we = '1' then
      case s_ctl_addr is 
        when C_ADDR_COUNTER =>
          r_timout_counter <= s_ctl_m2s.dat(g_counter_width-1 downto 0);
        when others => 
          null;
      end case;
    end if;
    if rst_n_i = '0' then
      if natural(unsigned(r_timout_counter)'HIGH) <= g_default_timeout then
        r_timout_counter <= std_logic_vector(to_unsigned(g_default_timeout, r_timout_counter'length));
      else
        r_timout_counter <= (others => '1');
      end if;
    end if;
  end if;
end process;

p_ctl_read: process(clk_i)
begin
  if rising_edge(clk_i) then
    if sig_ctl_m2s.cyc = '1' and sig_ctl_m2s.stb = '1' and sig_ctl_s2m.stall = '0' then      
      case s_ctl_addr is 
        when C_ADDR_ADDR =>
          sig_ctl_s2m.dat <= r_status_addr;
        when C_ADDR_STATUS =>
          sig_ctl_s2m.dat <= (0=>r_status_rd_err, 1=> r_status_rd_timeout, 2=>r_status_wr_err, 3=>r_status_wr_timeout, others => '0');
        when others =>
          sig_ctl_s2m.dat <= (others => '0');
      end case;
    end if;

    if rst_n_i = '0' then
      sig_ctl_s2m.dat <= (others => '0');
    end if;
  end if;
end process;
      


p_ctl_ack: process(clk_i)
begin
  if rising_edge(clk_i) then
    if sig_ctl_m2s.cyc = '1' and sig_ctl_m2s.stb = '1' and sig_ctl_s2m.stall = '0' then
      sig_ctl_s2m.ack <= '1';
    else  
      sig_ctl_s2m.ack <= '0';
    end if;
    sig_ctl_s2m.stall <= '0';
    if rst_n_i = '0' then
      sig_ctl_s2m.stall <= '1';
      sig_ctl_s2m.err <= '0';
      sig_ctl_s2m.ack <= '0';
      sig_ctl_s2m.rty <= '0';
    end if;
  end if;
end process;


end Behavioral;
