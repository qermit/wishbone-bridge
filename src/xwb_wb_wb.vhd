-------------------------------------------------------------------------------
-- Title      :
-- Project    : Wishbone Bridge
-------------------------------------------------------------------------------
-- File       : xwb_wb_wb.vhd
-- Authors    : Piotr Miedzik <qermit@sezamkowa.net>
-- Company    :
-- Created    : 2020-09-30
-- Last update: 2020-09-30
-- License    : This is a PUBLIC DOMAIN code, published under
--              Creative Commons CC0 license
-- Platform   :
-- Standard   : VHDL'93/02
-------------------------------------------------------------------------------
-- Description: 
-------------------------------------------------------------------------------
-- Copyleft (ↄ) 2020 Piotr Miedzik 
-------------------------------------------------------------------------------
-- Revisions  : see git commit log
-------------------------------------------------------------------------------
-- External requirements :
-------------------------------------------------------------------------------
-- Implementation details
-------------------------------------------------------------------------------
--
-------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
use std.textio.all;

library general_cores;
use general_cores.wishbone_pkg.all;

use work.wishbone_bridge_pkg.all;

entity xwb_wb_wb is
  generic(
    -- Address of the slaves connected
    g_zeropage_address     : t_wishbone_address_array;
    g_zeropage_mask        : t_wishbone_address_array;
    -- output interface mode
    g_wishbone_mode        : t_wishbone_interface_mode      := PIPELINED;
    g_wishbone_granularity : t_wishbone_address_granularity := BYTE;
    g_timeout: boolean := true);
  port(
    clk_sys_i     : in  std_logic;
    rst_n_i       : in  std_logic;
    -- slave connection from ... , PIPELINED
    s_input_m2s: in  t_wishbone_slave_in;
    s_input_s2m: out t_wishbone_slave_out;
    -- Master connection to the Wishbone slaves
    m_output_m2s: out t_wishbone_master_out;
    m_output_s2m: in  t_wishbone_master_in;
    -- additional zero page, PIPELINED 
    m_zeropage_m2s: out t_wishbone_master_out_array(g_zeropage_address'length -1 downto 0);
    m_zeropage_s2m: in  t_wishbone_master_in_array(g_zeropage_address'length -1 downto 0)
  );
end xwb_wb_wb;


architecture rtl of xwb_wb_wb is
  constant c_num_zeropage: natural := g_zeropage_address'length -1;
  -- 0x00 -> last_addr
  -- 0x04 -> flags
  -- 0x08 -> counter 
  -- 0x0C


  constant c_zeropage_address_internal: t_wishbone_address_array(0 downto 0) := ( 0 => x"00000000");
  constant c_zeropage_mask_internal: t_wishbone_address_array(0 downto 0) := ( 0 => x"fffffff0");
  constant c_zeropage_address: t_wishbone_address_array(g_zeropage_address'length downto 0) := c_zeropage_address_internal & g_zeropage_address;
  constant c_zeropage_mask: t_wishbone_address_array(g_zeropage_mask'length downto 0) := c_zeropage_mask_internal & g_zeropage_mask;

  function f_zeropage_check_conflicts(p_zeropage_address: t_wishbone_address_array; p_zeropage_mask: t_wishbone_address_array) return boolean is
    variable I_min, I_max, J_min, J_max: t_wishbone_address;
  begin
    for I in 0 to p_zeropage_address'length - 2 loop
      I_min := p_zeropage_address(I) and p_zeropage_mask(I);
      I_max := p_zeropage_address(I) or (not p_zeropage_mask(I));
      for J in I + 1 to p_zeropage_address'length - 1 loop
        J_min := p_zeropage_address(J) and p_zeropage_mask(J);
        J_max := p_zeropage_address(J) or (not p_zeropage_mask(J));
        if not ((unsigned(J_max) < unsigned(I_min)) or (unsigned(I_max) < unsigned(J_min))) then
          return false;
        end if;
      end loop;
    end loop;
    return true;
  end f_zeropage_check_conflicts;

  function f_zeropage_max_addr(p_zeropage_address: t_wishbone_address_array; p_zeropage_mask: t_wishbone_address_array) return t_wishbone_address is
    variable v_address: t_wishbone_address := p_zeropage_address(0);
    variable I_min, I_max, J_min, J_max: t_wishbone_address;
  begin
    for I in 0 to p_zeropage_address'length - 1 loop
      I_min := p_zeropage_address(I) and p_zeropage_mask(I);
      I_max := p_zeropage_address(I) or (not p_zeropage_mask(I));
      if  unsigned(v_address) < unsigned(I_max) then
          v_address := I_max;
      end if;
    end loop;
    return v_address;
  end f_zeropage_max_addr;

  impure function f_zeropage_address_space(p_zeropage_max_address: t_wishbone_address) return t_wishbone_address_array is
    variable v_mask: t_wishbone_address_array(0 downto 0);
  begin
    v_mask(0) :=  std_logic_vector(to_signed(-(2**integer(ceil(log2(real(to_integer(unsigned(p_zeropage_max_address))+1))))),32));
    return v_mask;
  end f_zeropage_address_space;

  constant c_max_addr: t_wishbone_address := f_zeropage_max_addr(c_zeropage_address, c_zeropage_mask);

  constant c_zeropage_address_front: t_wishbone_address_array(0 downto 0) := ( 0 => x"00000000");
  constant c_zeropage_mask_front: t_wishbone_address_array(0 downto 0) := f_zeropage_address_space(c_max_addr);

  signal sig_input_m2s: t_wishbone_slave_in_array(0 downto 0);
  signal sig_input_s2m: t_wishbone_slave_out_array(0 downto 0);

  signal sig_zeropage_m2s: t_wishbone_master_out_array(0 downto 0);
  signal sig_zeropage_s2m: t_wishbone_master_in_array(0 downto 0);

  signal sig_zeropage_crossbar_m2s: t_wishbone_master_out_array(c_zeropage_address'length - 1 downto 0);
  signal sig_zeropage_crossbar_s2m: t_wishbone_master_in_array(c_zeropage_address'length - 1 downto 0);

  signal sig_fallback_m2s: t_wishbone_master_out;
  signal sig_fallback_s2m: t_wishbone_master_in;

  signal sig_fallback_zeropage_m2s: t_wishbone_master_out;
  signal sig_fallback_zeropage_s2m: t_wishbone_master_in;

  signal sig_fallback_output_m2s: t_wishbone_master_out;
  signal sig_fallback_output_s2m: t_wishbone_master_in;

  signal sig_output_m2s: t_wishbone_master_out;
  signal sig_output_s2m: t_wishbone_master_in;
  
  constant c_counter_width : natural := 8;
  signal sig_timeout_counter: std_logic_vector(c_counter_width-1 downto 0);
begin

assert f_zeropage_check_conflicts(c_zeropage_address, c_zeropage_mask) report "Zeropage address map" severity error;

sig_input_m2s(0) <= s_input_m2s;
s_input_s2m <= sig_input_s2m(0);

u_front_crossbar: entity work.xwb_crossbar_fallback
  generic map (
    g_num_masters => 1,
    g_num_slaves  => 1,
    g_registered  => false,
    g_address     => c_zeropage_address_front,
    g_mask        => c_zeropage_mask_front
    )
  port map (
    clk_sys_i     => clk_sys_i,
    rst_n_i       => rst_n_i,
    -- Master connections (INTERCON is a slave)
    slave_i       => sig_input_m2s,
    slave_o       => sig_input_s2m,
    -- Slave connections (INTERCON is a master)
    master_i     => sig_zeropage_s2m,
    master_o     => sig_zeropage_m2s,
    -- fallback connectios (INTERCON is a master)
    fallback_i    => sig_fallback_s2m,
    fallback_o    => sig_fallback_m2s
  );
  

  
  u_bridge: entity work.xwb_bridge
  generic map(
    g_counter_width =>  c_counter_width,
    g_default_timeout => 8
  )
  port map(
    clk_i        => clk_sys_i,
    rst_n_i       => rst_n_i,
    
    s_input_m2s => sig_fallback_m2s,
    s_input_s2m => sig_fallback_s2m,
    -- Master connection to the Wishbone slaves
    m_output_m2s => sig_fallback_output_m2s,
    m_output_s2m => sig_fallback_output_s2m,
    -- additional zero page

    s_ctl_m2s => sig_zeropage_crossbar_m2s(c_zeropage_address'length - 1),
    s_ctl_s2m => sig_zeropage_crossbar_s2m(c_zeropage_address'length - 1),
    
    counter_o => sig_timeout_counter
  );
  
GEN_TIMEOUT: if g_timeout = true generate  
  u_xwb_timeout: entity work.xwb_timeout
  generic map(
    g_counter_width => c_counter_width
  )
  port map (
    clk_i        => clk_sys_i,
    rst_n_i       => rst_n_i,
    
    s_input_m2s => sig_fallback_output_m2s,
    s_input_s2m => sig_fallback_output_s2m,
    -- Master connection to the Wishbone slaves
    m_output_m2s => sig_output_m2s,
    m_output_s2m => sig_output_s2m,
    -- additional zero page 
    timeout_counter_i => sig_timeout_counter
  );
end generate;

GEN_NO_TIMEOUT: if g_timeout = false generate
begin
  sig_output_m2s  <= sig_fallback_output_m2s;
  sig_fallback_output_s2m <= sig_output_s2m;

end generate;

  u_out_adapter: entity work.xwb_adapter
    generic  map(
      g_master_mode        => g_wishbone_mode,
      g_master_granularity => g_wishbone_granularity,
      g_slave_mode         => PIPELINED,
      g_slave_granularity  => BYTE)
    port map (
      clk_sys_i     => clk_sys_i,
      rst_n_i       => rst_n_i,
    
      slave_i    => sig_output_m2s,
      slave_o    => sig_output_s2m,
      master_i   => m_output_s2m,
      master_o   => m_output_m2s
    );


 u_zeropage_crossbar: entity work.xwb_crossbar_fallback
  generic map (
    g_num_masters => 1,
    g_num_slaves  => c_zeropage_address'length,
    g_registered  => false,
    g_address     => c_zeropage_address,
    g_mask        => c_zeropage_mask
    )
  port map (
    clk_sys_i     => clk_sys_i,
    rst_n_i       => rst_n_i,
    -- Master connections (INTERCON is a slave)
    slave_i       => sig_zeropage_m2s,
    slave_o       => sig_zeropage_s2m,
    -- Slave connections (INTERCON is a master)
    master_i     => sig_zeropage_crossbar_s2m,
    master_o     => sig_zeropage_crossbar_m2s,
    -- fallback connectios (INTERCON is a master)
    fallback_i    => sig_fallback_zeropage_s2m,
    fallback_o    => sig_fallback_zeropage_m2s
  );
  sig_fallback_zeropage_s2m.ack <= '0';
  sig_fallback_zeropage_s2m.rty <= '0';
  sig_fallback_zeropage_s2m.dat <= (others => '0');
  sig_fallback_zeropage_s2m.stall <= '0';


  p_fallback_zeropage: process(clk_sys_i)
  begin
    if rising_edge(clk_sys_i) then
      if sig_fallback_zeropage_m2s.cyc = '1' and sig_fallback_zeropage_m2s.stb = '1' then
        sig_fallback_zeropage_s2m.err <= '1';
      else
        sig_fallback_zeropage_s2m.err <= '0';
      end if;
    end if;
  end process;


  m_zeropage_m2s <= sig_zeropage_crossbar_m2s(c_zeropage_address'length - 2 downto 0);
  sig_zeropage_crossbar_s2m(c_zeropage_address'length - 2 downto 0) <= m_zeropage_s2m;

end rtl;

