----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/26/2020 10:52:12 AM
-- Design Name: 
-- Module Name: tb_xwb_crossbar - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
library general_cores;
use general_cores.wishbone_pkg.all;
use work.pcie3_mm_pkg.all;

entity tb_xwb_wb_wb is
--  Port ( );
end tb_xwb_wb_wb;

architecture Behavioral of tb_xwb_wb_wb is
constant PERIOD : time := 4ns;  

signal clk_i : std_logic;
signal rst_n_i : std_logic;

constant      c_address     : t_wishbone_address_array(1 downto 0) := (x"00000010", x"00000030");
constant      c_mask        : t_wishbone_address_array(1 downto 0) := (x"FFFFFFF0", x"FFFFFFF0");

constant      c_num_masters : natural := 1;
constant      c_num_slaves  : natural := 2;

signal      s_input_m2s   : t_wishbone_slave_in;
signal      s_input_s2m   : t_wishbone_slave_out;

signal      m_output_s2m  : t_wishbone_master_in;
signal      m_output_m2s  : t_wishbone_master_out;

signal      m_zeropage_m2s: t_wishbone_master_out_array(1 downto 0);
signal      m_zeropage_s2m: t_wishbone_master_in_array(1 downto 0);

signal      m_wb_m2s_0: t_wishbone_a32_d32_m2s;
signal      m_wb_s2m_0: t_wishbone_a32_d32_s2m;
signal      m_wb_m2s_1: t_wishbone_a32_d32_m2s;
signal      m_wb_s2m_1: t_wishbone_a32_d32_s2m;



component wb_dummy_slave is
    Port ( clk_i : in STD_LOGIC;
    rst_n_i : in std_logic;
               -- Wishbone master interface
           m_wb_m2s: in t_wishbone_a32_d32_m2s;
           m_wb_s2m: out t_wishbone_a32_d32_s2m
    );       
end  component wb_dummy_slave;

component xwb_wb_wb is
  generic(
    -- Address of the slaves connected
    g_zeropage_address     : t_wishbone_address_array;
    g_zeropage_mask        : t_wishbone_address_array;
    -- output interface mode
    g_wishbone_mode        : t_wishbone_interface_mode      := PIPELINED;
    g_wishbone_granularity : t_wishbone_address_granularity := BYTE);
  port(
    clk_sys_i     : in  std_logic;
    rst_n_i       : in  std_logic;
    -- slave connection from ... , PIPELINED
    s_input_m2s: in  t_wishbone_slave_in;
    s_input_s2m: out t_wishbone_slave_out;
    -- Master connection to the Wishbone slaves
    m_output_m2s: out t_wishbone_master_out;
    m_output_s2m: in  t_wishbone_master_in;
    -- additional zero page, PIPELINED 
    m_zeropage_m2s: out t_wishbone_master_out_array(g_zeropage_address'length -1 downto 0);
    m_zeropage_s2m: in  t_wishbone_master_in_array(g_zeropage_address'length -1 downto 0)
  );
end component xwb_wb_wb;

begin

     clk_stim: process
begin
  clk_i <= '0';
  wait for PERIOD/2;
  clk_i <= '1';
  wait for PERIOD/2;
end process;

rst_stim: process
begin
  rst_n_i <= '0';
  wait for PERIOD*4;
  rst_n_i <= '1';
  wait;
end process;



    m_wb_m2s_0.cyc <= m_zeropage_m2s(0).cyc;
    m_wb_m2s_0.stb <= m_zeropage_m2s(0).stb;
    m_wb_m2s_0.adr <= m_zeropage_m2s(0).adr;
    m_wb_m2s_0.sel <= m_zeropage_m2s(0).sel;
    m_wb_m2s_0.we  <= m_zeropage_m2s(0).we;
    m_wb_m2s_0.dat <= m_zeropage_m2s(0).dat;
    
    m_wb_m2s_1.cyc <= m_zeropage_m2s(1).cyc;
    m_wb_m2s_1.stb <= m_zeropage_m2s(1).stb;
    m_wb_m2s_1.adr <= m_zeropage_m2s(1).adr;
    m_wb_m2s_1.sel <= m_zeropage_m2s(1).sel;
    m_wb_m2s_1.we  <= m_zeropage_m2s(1).we;
    m_wb_m2s_1.dat <= m_zeropage_m2s(1).dat;

    m_zeropage_s2m(0).ack   <= m_wb_s2m_0.ack;
    m_zeropage_s2m(0).err   <= m_wb_s2m_0.err;
    m_zeropage_s2m(0).rty   <= m_wb_s2m_0.rty;
    m_zeropage_s2m(0).stall <= m_wb_s2m_0.stall;
    m_zeropage_s2m(0).dat   <= m_wb_s2m_0.dat;

    m_zeropage_s2m(1).ack   <= m_wb_s2m_1.ack;
    m_zeropage_s2m(1).err   <= m_wb_s2m_1.err;
    m_zeropage_s2m(1).rty   <= m_wb_s2m_1.rty;
    m_zeropage_s2m(1).stall <= m_wb_s2m_1.stall;
    m_zeropage_s2m(1).dat   <= m_wb_s2m_1.dat;


    m_output_s2m.ack   <= '0';
    m_output_s2m.err   <= '0';
    m_output_s2m.rty   <= '0';
    m_output_s2m.stall <= '0';
    m_output_s2m.dat   <= x"00000000";

uut: xwb_wb_wb 
  generic map(
    -- Address of the slaves connected
    g_zeropage_address     => c_address,
    g_zeropage_mask        => c_mask
)
  port map(
    clk_sys_i     => clk_i,
    rst_n_i       => rst_n_i,
    -- slave connection from ... , PIPELINED
    s_input_m2s  => s_input_m2s,
    s_input_s2m  => s_input_s2m,
    -- Master connection to the Wishbone slaves
    m_output_m2s => m_output_m2s,
    m_output_s2m => m_output_s2m,
    -- additional zero page, PIPELINED 
    m_zeropage_m2s => m_zeropage_m2s,
    m_zeropage_s2m => m_zeropage_s2m
  );



u_dummy_0: wb_dummy_slave 
  Port map(
    clk_i => clk_i,
    rst_n_i => rst_n_i,
    -- Wishbone master interface
    m_wb_m2s => m_wb_m2s_0,
    m_wb_s2m => m_wb_s2m_0
    );

u_dummy_1: wb_dummy_slave 
  Port map(
    clk_i => clk_i,
    rst_n_i => rst_n_i,
    -- Wishbone master interface
    m_wb_m2s => m_wb_m2s_1,
    m_wb_s2m => m_wb_s2m_1
    );


p1: process
begin
  s_input_m2s.cyc <= '0';
  s_input_m2s.stb <= '0';
  s_input_m2s.sel <= "0000";
  s_input_m2s.we <= '0';
  s_input_m2s.dat <= x"00000000";
  s_input_m2s.adr <= x"00000000";
  wait for  PERIOD * 10;
  s_input_m2s.cyc <= '1';
  s_input_m2s.stb <= '1';
  s_input_m2s.sel <= "1111";
  s_input_m2s.we <= '1';
  s_input_m2s.dat <= x"12341234";
  s_input_m2s.adr <= x"0000003F";
  wait for  PERIOD;
  s_input_m2s.stb <= '0';
  s_input_m2s.cyc <= '0';
  wait for  PERIOD;
  s_input_m2s.cyc <= '1';
  s_input_m2s.stb <= '1';
  s_input_m2s.adr <= x"00000040";
--  s_input_m2s(0).adr <= x"00001000";
  wait for  PERIOD;
  s_input_m2s.stb <= '0';
  wait for  PERIOD*10;
  s_input_m2s.cyc <= '0';

  wait for  PERIOD * 10;
  s_input_m2s.cyc <= '1';
  s_input_m2s.stb <= '1';
  s_input_m2s.sel <= "1111";
  s_input_m2s.we <= '1';
  s_input_m2s.dat <= x"12341234";
  s_input_m2s.adr <= x"00001100";
  wait for  PERIOD;
  s_input_m2s.stb <= '1';
  s_input_m2s.dat <= x"12341235";
  s_input_m2s.adr <= x"00001104";
--  s_input_m2s(0).adr <= x"00001000";
  wait for  PERIOD;
  s_input_m2s.stb <= '0';
  wait for  PERIOD*10;
  s_input_m2s.cyc <= '0';
  
  wait;
end process;
    
    
end Behavioral;
