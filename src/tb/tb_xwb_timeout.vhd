----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 09/26/2020 10:52:12 AM
-- Design Name: 
-- Module Name: tb_xwb_crossbar - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library general_cores;
use general_cores.wishbone_pkg.all;
use work.pcie3_mm_pkg.all;

entity tb_xwb_timeout is
--  Port ( );
end tb_xwb_timeout;

architecture Behavioral of tb_xwb_timeout is
constant PERIOD : time := 4ns;  

signal clk_i : std_logic;
signal rst_n_i : std_logic;

signal      s_input_m2s   : t_wishbone_slave_in;
signal      s_input_s2m   : t_wishbone_slave_out;

signal      m_output_s2m  : t_wishbone_master_in;
signal      m_output_m2s  : t_wishbone_master_out;


component xwb_timeout is
  generic (
    g_counter_width: natural := 8
  );
    Port ( clk_i : in STD_LOGIC;
           rst_n_i : in STD_LOGIC;

           s_input_m2s : in  t_wishbone_slave_in;
           s_input_s2m : out t_wishbone_slave_out;

           m_output_s2m : in  t_wishbone_master_in;
           m_output_m2s : out t_wishbone_master_out;
           
           timeout_counter_i: in std_logic_vector(g_counter_width-1 downto 0)
      );
end component xwb_timeout;

begin

     clk_stim: process
begin
  clk_i <= '0';
  wait for PERIOD/2;
  clk_i <= '1';
  wait for PERIOD/2;
end process;

rst_stim: process
begin
  rst_n_i <= '0';
  wait for PERIOD*4;
  rst_n_i <= '1';
  wait;
end process;

    m_output_s2m.ack   <= '0';
    m_output_s2m.err   <= '0';
    m_output_s2m.rty   <= '0';
    m_output_s2m.stall <= '0';
    m_output_s2m.dat   <= x"00000000";

uut:xwb_timeout
  generic map(
    g_counter_width => 8
  )
  Port map( clk_i => clk_i,
           rst_n_i => rst_n_i,

           s_input_m2s => s_input_m2s,
           s_input_s2m => s_input_s2m,

           m_output_s2m => m_output_s2m,
           m_output_m2s => m_output_m2s,
           
           timeout_counter_i => x"0f"
  );



p1: process
begin
  s_input_m2s.cyc <= '0';
  s_input_m2s.stb <= '0';
  s_input_m2s.sel <= "0000";
  s_input_m2s.we <= '0';
  s_input_m2s.dat <= x"00000000";
  s_input_m2s.adr <= x"00000000";
  wait for  PERIOD * 10;
  s_input_m2s.cyc <= '1';
  s_input_m2s.stb <= '1';
  s_input_m2s.sel <= "1111";
  s_input_m2s.we <= '1';
  s_input_m2s.dat <= x"12341234";
  s_input_m2s.adr <= x"00001000";
  wait for  PERIOD;
  s_input_m2s.stb <= '1';
  s_input_m2s.adr <= x"00001004";
--  s_input_m2s(0).adr <= x"00001000";
  wait for  PERIOD;
  s_input_m2s.stb <= '0';  
  wait;
end process;
    
    
end Behavioral;
